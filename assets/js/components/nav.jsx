import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Nav(props) {
  return (
    <header className="main-header">
		<div className="container">
		<ul>
	        <li>
	          <Link to="/">Usuarios</Link>
	        </li>
	        <li>
	          <Link to="/about">About</Link>
	        </li>
	        <li>
	          <Link to="/topics">Topics</Link>
	        </li>
	    </ul>

		</div>
     </header>

  )
}

export default Nav;