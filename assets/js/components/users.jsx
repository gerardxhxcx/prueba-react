import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Users extends React.Component {
	
	constructor(props){
		super(props)
	}

	render() {
		console.log(this)
		return (
			<ul className="Users">
				{this.props.route.users.map((userData, index) => {
					<li key={index}>{userData.username}</li>
				})}
			</ul>
		)
	}
}

/*{this.props.data.teachers.map((teacherData, index) => {
				return <Teacher {...teacherData} key={index}/>
			})}*/
export default Users