import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Nav from "./nav.jsx"
import Users from "./users.jsx"


const Routes = (props) => (
  <Router>
    <div>
      <Nav/>

      <Route exact path="/" component={Users} users={props.users} />
    </div>
  </Router>
);

export default Routes;