import '../stylus/main.styl'
import '../stylus/asd.styl'

console.log('asd')
import React from 'react'
import { render } from 'react-dom'

import Routes from './components/routes.jsx'
/*
import Teachers from './components/teachers.js'
import data from './teachers.json'
*/

class App extends React.Component {
	constructor(props){
		super(props)
		this.state = {users: []}
	}
	componentWillMount() {
		fetch('http://jsonplaceholder.typicode.com/users')
			.then((response) => {
				return response.json()
			})
			.then((users) => {
				this.setState({ users: users })
			})
	}

	render() {
		if (this.state.users.length > 0) {
			return <Routes data={this.state} />
		} 
		else {
			return <p className="text-center">Cargando usuarios...</p>
		}
	}
}


render(<App/>, document.getElementById('app'))
