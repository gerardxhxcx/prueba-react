const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

const config = {
   entry: {
      modules: [
        'react',
        'react-dom',
      ],
   },
   output: {
      path: path.resolve(__dirname, 'public','js'),
      filename: '[name].js',
      library: '[name]',
   },
   plugins: [
      //new webpack.optimize.UglifyJsPlugin(),
      new webpack.DllPlugin({
         name: "[name]",
         path: path.join(__dirname, "[name]-manifest.json")
      })
   ]
}

module.exports = config
