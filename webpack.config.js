const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const webpack = require('webpack');
const ReplaceCssPath = require('replace-css-path');

const config = {
   entry: {
      main: path.resolve(__dirname, 'assets/js/main.js'),
   },
   output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'js/[name].js',
      publicPath: path.resolve(__dirname, 'public')+'/',
      chunkFilename: 'js/[id].[chunkhash].js',
   },
   devtool: 'source-map',
   module: {
      rules: [
         {
            test: /\.(js|jsx)$/,
            use: {
               loader: 'babel-loader',
               options: {
                 presets: ['env', 'react'],
                 plugins: ['syntax-dynamic-import'],
               }
            }
         },
         {
           test: /\.(jpg|png|gif)$/,
           use: {
             loader: 'url-loader',
             options: {
               limit: 100, //  limit for base64 in bytes
               name: 'images/[name].[ext]'
             }
           }
        },
        {
           test: /\.(woff|woff2|eot|ttf|svg)$/,
           use: {
             loader: 'url-loader',
             options: {
               limit: 100,
               name: 'fonts/[name].[ext]'
             }
           }
        },
        {
         test: /\.(mp4|webm)$/,
         use: {
            loader: 'url-loader',
            options: {
               limit: 100,
               name: 'videos/[name].[hash].[ext]'
            }
         }
         },
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
               use: [
                   {
                      loader: 'css-loader',
                      options: { minimize: true }
                   }
               ]
            }),
        },
        {
           test: /\.styl$/,
           use: ExtractTextPlugin.extract({
             use: [
               'css-loader',
               {
                 loader: 'stylus-loader',
                 options: {
                     use: [
                        require('nib'),
                        require('rupture')
                     ],
                     import: [
                        '~nib/lib/nib/index.styl',
                        '~rupture/rupture/index.styl'
                     ],
                     sourceMap: true
                 }
               }
             ]
           }),
        },
      ]
   },
   plugins: [
      new ExtractTextPlugin("css/[name].css"),
      new OptimizeCssAssetsPlugin({
			assetNameRegExp: /\.css$/,
			cssProcessorOptions: { discardComments: { removeAll: true } }
		}),
      new ReplaceCssPath('/public/css/main.css', path.resolve(__dirname, 'public'), '../'),
      //new webpack.optimize.UglifyJsPlugin(),
      new webpack.DllReferencePlugin({
         manifest: require('./modules-manifest.json')
      }),
   ]
}

module.exports = config
