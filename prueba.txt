Utilizando la siguiente API: (https://jsonplaceholder.typicode.com/todos), crea una SPA construida con Javascript en el framework de tu preferencia. Esta app debe encargarse de crear distintos usuarios y administrar los "To Do" (Tareas) de cada uno de ellos.
La SPA debe cumplir con :

USUARIO
-------------------------------------
- Crear usuario.
- Listar usuarios.

TODOS
-------------------------------------
- Listar los ToDos del usuario.
- Crear ToDo al usuario.
- Editar ToDo del usuario.
- Eliminar ToDo del usuario.
- Completar ToDo.


Puntos Adicionales:

Sumarás puntos adicionales por:
- Utilizar React.
- Implementar estilos y/o animaciones.
- Implementar routing.
- Desplegar la app en GitHub Pages.
- Filtrar ToDos por usuario, por título y/o por estado completado. 

